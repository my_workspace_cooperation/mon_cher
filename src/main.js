import { createApp } from 'vue'
import VueGoogleMaps from '@fawmi/vue-google-maps'
import App from './App.vue'

const app = createApp(App);
app.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyCCVbxN2ZrlUtVlfRqwuDLoVxrfudZ0b2c',
    },
}).mount('#app')
