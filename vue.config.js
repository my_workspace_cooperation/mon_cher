module.exports = {
    configureWebpack: config => {
        config.output.globalObject = "this"
    },
    publicPath: process.env.NODE_ENV === 'production'
        ? '/mozaik_group/'
        : '/'
}
